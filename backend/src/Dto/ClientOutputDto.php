<?php

declare(strict_types=1);

namespace App\Dto;

use App\Entity\Client;

class ClientOutputDto
{
    private ?int $id = null;

    private array $clientsArray;

    public function __construct($clients)
    {
        foreach ($clients as $client) {

            if (!$client instanceof Client) {
                continue;
            }

            $this->clientsArray[] = [
                'id' => $client->getId(),
                'name' => $client->getName(),
                'inn' => $client->getInn(),
                'kpp' => $client->getKpp(),
                'ogrn' => $client->getOgrn(),
                'u_address' => $client->getUAddress(),
                'f_address' => $client->getFAddress(),
                'email' => $client->getEmail(),
                'fill_name' => $client->getFillName(),
                'date_create' => $client->getDateCreate()->format('Y-m-d'),
                'unscrupulous' => $client->getUnscrupulous(),
                'debtor' => $client->getDebtor()
            ];
        }
    }

    /**
     * @return array
     */
    public function getClientsArray(): array
    {
        return $this->clientsArray;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return ClientOutputDto
     */
    public function setId(?int $id): ClientOutputDto
    {
        $this->id = $id;
        return $this;
    }

}
