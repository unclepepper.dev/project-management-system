<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 40; $i++) {
            $client = new Client();
            $client->setName('product ' . $i);
            $client->setInn(mt_rand(10, 1000000));
            $client->setKpp(mt_rand(10, 1000000));
            $client->setOgrn(mt_rand(10, 1000000));
            $client->setUAddress('Юридический адрес' . $i);
            $client->setFAddress('Фактический адрес' . $i);
            $client->setFillName('Иванов Иван ' . $i);
            $client->setEmail('ivanov@' . $i . '.ru');
            $client->setUnscrupulous(false);
            $client->setDebtor(true);

            $manager->persist($client);
        }

        $manager->flush();
    }
}
