<?php

//declare(strict_types=1);

namespace App\Controller;

use App\Entity\Client;
use App\Service\ClientService;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientApiController extends AbstractController
{
    public function __construct(
        private ClientService $clientService,
        private EntityManagerInterface $manager
    )
    {}

    #[Route('/api/client', name: 'app_api_client')]
    public function index(): JsonResponse|Exception
    {
        try {
            $clients = $this->clientService->getClientDto()->getClientsArray();
            $response = $this->response($clients,
                Response::HTTP_OK,
//                ['Access-Control-Allow-Origin' => '*']
            );
        } catch (Exception $e){
            $response = $e;
        }
        return $response;
    }

    #[Route('/api/client-save', name: 'app_api_client_add', methods: 'post')]
    public function save(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $this->clientService->saveClient($data);

        return $this->response(
            ['success' =>200],
            Response::HTTP_CREATED

        );

    }

    public function response($data, $status = 200, $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }
}
