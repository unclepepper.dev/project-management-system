<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\ClientOutputDto;
use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;


class ClientService
{
    public function __construct(
        private ClientRepository $repository,
        private EntityManagerInterface $manager
    )
    {}

    public function getClient(): array
    {
        /** @var array $clients */
        return $this->repository->findAll();
    }

    public function getClientDto(): ClientOutputDto
    {
        return new ClientOutputDto($this->getClient());
    }

    public function saveClient(array $data):bool
    {
        $name = empty($data['name']) ? null : $data['name'] ;
        $inn =  empty($data['inn']) ? null : $data['inn'];
        $kpp = empty($data['kpp']) ? null : $data['kpp'];
        $ogrn = empty($data['ogrn']) ? null : $data['ogrn'];
        $uAddress = empty($data['u_address']) ? null : $data['u_address'];
        $fAddress = empty($data['f_address']) ? null : $data['f_address'];
        $fillName = empty($data['fill_name']) ? null : $data['fill_name'];
        $email =  empty($data['email']) ? null : $data['fill_name'];
        $unscrupulous = $data['unscrupulous'] === 'Да';
        $debtor = $data['debtor'] === 'Да';

        $client = new Client();
        $client->setName($name);
        $client->setInn($inn);
        $client->setKpp($kpp);
        $client->setOgrn($ogrn);
        $client->setUAddress($uAddress);
        $client->setFAddress($fAddress);
        $client->setFillName($fillName);
        $client->setEmail($email);
        $client->setUnscrupulous($unscrupulous);
        $client->setDebtor($debtor);


        try {
            $this->manager->persist($client);
            $this->manager->flush($client);
            $bool = true;
        } catch (Exception $e){
            $response = $e;
            $bool = false;
        }
        return $bool;
    }
}
