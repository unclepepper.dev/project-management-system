<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;



#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\Column(type: "uuid", unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    protected UuidInterface $id;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    private ?int $inn = null;

    #[ORM\Column(nullable: true)]
    private ?int $kpp = null;

    #[ORM\Column(nullable: true)]
    private ?int $ogrn = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $u_address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $f_address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $fillName = null;

    #[ORM\Column(nullable: true)]
    #[Gedmo\Timestampable(on: 'create')]
    private ?\DateTimeImmutable $date_create = null;

    #[ORM\Column]
    private bool $unscrupulous = false;

    #[ORM\Column]
    private bool $debtor = false;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email = null;

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInn(): ?int
    {
        return $this->inn;
    }

    public function setInn(?int $inn): self
    {
        $this->inn = $inn;

        return $this;
    }

    public function getKpp(): ?int
    {
        return $this->kpp;
    }

    public function setKpp(?int $kpp): self
    {
        $this->kpp = $kpp;

        return $this;
    }

    public function getOgrn(): ?int
    {
        return $this->ogrn;
    }

    public function setOgrn(?int $ogrn): self
    {
        $this->ogrn = $ogrn;

        return $this;
    }

    public function getUAddress(): ?string
    {
        return $this->u_address;
    }

    public function setUAddress(?string $u_address): self
    {
        $this->u_address = $u_address;

        return $this;
    }

    public function getFAddress(): ?string
    {
        return $this->f_address;
    }

    public function setFAddress(?string $f_address): self
    {
        $this->f_address = $f_address;

        return $this;
    }

    public function getFillName(): ?string
    {
        return $this->fillName;
    }

    public function setFillName(?string $fillName): self
    {
        $this->fillName = $fillName;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeImmutable
    {
        return $this->date_create;
    }

    public function setDateCreate(?\DateTimeImmutable $date_create): self
    {
        $this->date_create = $date_create;

        return $this;
    }

    public function isUnscrupulous(): ?bool
    {
        return $this->unscrupulous;
    }
    public function getUnscrupulous(): string
    {
        return $this->isUnscrupulous() ? "Да" : "Нет";
    }
    public function setUnscrupulous(bool $unscrupulous): self
    {
        $this->unscrupulous = $unscrupulous;

        return $this;
    }

    public function isDebtor(): ?bool
    {
        return $this->debtor;
    }

    public function getDebtor(): string
    {
        return $this->isDebtor() ? "Да" : "Нет";
    }
    public function setDebtor(bool $debtor): self
    {
        $this->debtor = $debtor;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
