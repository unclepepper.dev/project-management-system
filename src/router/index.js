import { createRouter, createWebHistory } from 'vue-router'


const routes = [
  {
    path: '/',
    name: 'home',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "home" */ '../views/HomeView.vue')
  },
  {
    path: '/contracts',
    name: 'contracts',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "contracts" */ '../views/Contracts.vue'),
    children: [
        {
            path: 'addContract',
            name: 'addContract',
            meta: {layout: 'MainLayout'},
            component: () => import(/* webpackChunkName: "addContract" */ '../views/Add.vue')
        },
        {
            path: 'editContract/:id',
            name: 'editContract',
            meta: {layout: 'MainLayout'},
            component: () => import(/* webpackChunkName: "editContract" */ '../views/Edit.vue')
        },
    ]
  },
  {
    path: '/tasks',
    name: 'tasks',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "tasks" */ '../views/Tasks.vue')
  },
  {
    path: '/purchases',
    name: 'purchases',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "purchases" */ '../views/Purchases.vue')
  },
  {
    path: '/works',
    name: 'works',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "works" */ '../views/Works.vue')
  },
  {
    path: '/support',
    name: 'support',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "support" */ '../views/Support.vue')
  },
  {
    path: '/docs',
    name: 'docs',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "docs" */ '../views/Docs.vue')
  },
  {
    path: '/logistic',
    name: 'logistic',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "logistic" */ '../views/Logistic.vue')
  },
  {
    path: '/extensions',
    name: 'extensions',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "extensions" */ '../views/Extensions.vue')
  },
  {
    path: '/finance',
    name: 'finance',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "finance" */ '../views/Finance.vue')
  },
  {
    path: '/receivable',
    name: 'receivable',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "receivable" */ '../views/Receivable.vue')
  },
  {
    path: '/clients',
    name: 'clients',
    meta: {layout: 'MainLayout'},
    component: () => import(/* webpackChunkName: "clients" */ '../views/Clients.vue'),
    children: [
        {
            path: 'addClient',
            name: 'addClient',
            meta: {layout: 'MainLayout'},
            component: () => import(/* webpackChunkName: "addClient" */ '../views/Add.vue')
        },
        {
            path: 'edit/:id',
            name: 'edit',
            meta: {layout: 'MainLayout'},
            component: () => import(/* webpackChunkName: "edit" */ '../views/Edit.vue')
        },
    ]
  },
  
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
