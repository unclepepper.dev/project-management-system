export default {
    data(){
        return {
            inputItems: [],
            listItems: [],
        }
    },
    mounted(){
        this.inputItems = this.$store.state.entryData.inputItems;
        this.listItems = this.$store.state.entryData.listItems;
    },
    
}
