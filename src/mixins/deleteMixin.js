export default {
    methods: {
        delete(){
            if(this.listItems.filter(item => item.isChecked === true).length < 1){
                alert('Элементы для удаления не выбраны!')
            }else{
                if(confirm('Вы действительно хотите удалить выбранные Вами элементы ?')){
                    this.listItems.forEach(el => {
                        if(el.isChecked){
                        
                            this.listItems = this.listItems.filter(p => p.id !== el.id);  
                            this.$store.state.entryData.listItems = this.listItems;
                        }
                    })
                }
            }    
        },
    }
}
