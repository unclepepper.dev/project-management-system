export default {
    data(){
        return {
            isShow: false,
            isDisabled: false,
        }
    },
    methods: {
        showMessage(arr,rout){
            this.isShow = !this.isShow;
            setTimeout(() => {
            this.isShow = !this.isShow;
            this.$router.push({name: rout});
            this.clearFields(arr);
            }, 1000); 
        },
        clearFields(arr){
            arr.forEach(el => 
                el.value = ''
            )
        }
    }
}
