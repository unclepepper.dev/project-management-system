import {ref} from 'vue';
import {useRouter, useRoute } from 'vue-router';

export function useShowMessage(){

    const router = useRouter()
    const route = useRoute()
    const isShow = ref(false);
    
    function showMessage(arr,rout){
        isShow.value = !isShow.value;
        setTimeout(() => {
            isShow.value = !isShow.value;
            router.push({name: rout});
            arr.forEach(el => 
               el.value = ''
            )
        }, 1000); 
    }
    

    return {
        isShow,
    }
  
}     

