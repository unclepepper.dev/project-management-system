import NavBar from './NavBar.vue'; 
import InputTable from './InputTable.vue';
import EditTable from './EditTable.vue';
import ButtonSendForm from './ButtonSendForm.vue';
import ListDataTable from './ListDataTable.vue';
import CheckBox from './CheckBox.vue';
import ToolList from './ToolList.vue';
import ThElement from './ThElement.vue';
import FooterPages from './FooterPages.vue';



export default [
 NavBar,
 InputTable,
 EditTable,
 ButtonSendForm,
 ListDataTable,
 CheckBox,
 ToolList,
 ThElement,
 FooterPages
]
