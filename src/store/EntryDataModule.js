import axios from "axios";
export const EntryDataModule = {
  state: () => ({
    clientInputItems: 
    [
      {name:'name', short_title: 'Наименование', title: 'Наименование', isRequired:true, type: 'text'},
      {name:'inn',  short_title: 'ИНН', title: 'ИНН', isRequired:false, type: 'text'},
      {name:'kpp',  short_title: 'КПП', title: 'КПП', isRequired:false, type: 'text'},
      {name:'ogrn',  short_title: 'ОГРН', title: 'ОГРН', isRequired:false, type: 'text'},
      {name:'u_address',  short_title: 'Юр. адрес', title: 'Юридический адрес', isRequired:false, type: 'text'},
      {name:'f_address',  short_title: 'Факт. адрес', title: 'Фактический адрес', isRequired:false, type: 'text'},
      {name:'fill_name',  short_title: 'ФИО отв.', title: 'ФИО ответств.', isRequired:false, type: 'text'},
      {name:'email',  short_title: 'Эл. почта', title: 'Электронная почта', isRequired:false, type: 'text'},
      {name:'date_create',  short_title: 'Дата создания', title: 'Дата создания', isRequired:false, type: 'date'},
      {name:'unscrupulous',  short_title: 'Недоброс.', title: 'Недобросовестный', isRequired:false, type: 'select'},
      {name: 'debtor',  short_title: 'Должник', title: 'Должник', isRequired:false, type: 'select'},

    ],
    contractInputItems: 
    [
      {name:'date_create',  short_title: 'Д. создан.', title: 'Дата создания', isRequired:false, type: 'date'},
      {name:'date_conclusion',  short_title: 'Д. заключ.', title: 'Дата заключения', isRequired:false, type: 'date'},
      {name:'date_execution',  short_title: 'Д. испол.', title: 'Дата исполнения', isRequired:false, type: 'date'},
      {name:'date_renewal',  short_title: 'Д. продл.', title: 'Дата продления', isRequired:false, type: 'date'},
      {name:'date_repeat',  short_title: 'Д. повт. обсл.', title: 'Дата проведения повторного обследования', isRequired:false, type: 'date'},
      {name:'client', short_title: 'Клиент', title: 'Клиент', isRequired:true, type: 'text'},
      {name:'inn',  short_title: 'ИНН', title: 'ИНН', isRequired:false, type: 'text'},
      {name:'contract_number',  short_title: 'Ном. дог.', title: 'Номер договора', isRequired:false, type: 'text'},
      {name:'type_contract',  short_title: 'Тип дог.', title: 'Тип договора', isRequired:false, type: 'text'},
      {name:'type_works',  short_title: 'Тип работ', title: 'Тип работ', isRequired:false, type: 'text'},

      {name:'contract_form',  short_title: 'Форма дог.', title: 'Форма договора', isRequired:false, type: 'text'},
      {name:'counterparty',  short_title: 'Контр.', title: 'Контрагент', isRequired:false, type: 'text'},
      {name:'partner',  short_title: 'Партнер', title: 'Партнер', isRequired:false, type: 'text'},
      {name:'manager',  short_title: 'Менеджер', title: 'Менеджер', isRequired:false, type: 'text'},
      {name:'presale',  short_title: 'Пресейл', title: 'Пресейл', isRequired:false, type: 'text'},
      {name:'executer',  short_title: 'Исп.', title: 'Исполнитель', isRequired:false, type: 'text'},
      {name:'documents',  short_title: 'Док-ты', title: 'Документы', isRequired:false, type: 'text'},
      {name:'procurement',  short_title: 'Закупки', title: 'Закупки', isRequired:false, type: 'text'},
      {name:'works',  short_title: 'Работы', title: 'Работы', isRequired:false, type: 'text'},
      {name:'documentation',  short_title: 'Док-ия', title: 'Документация', isRequired:false, type: 'text'},
      {name:'support',  short_title: 'Тех. под.', title: 'Техническая поддержка', isRequired:false, type: 'text'},
      {name:'dispatch',  short_title: 'Отправки', title: 'Отправки', isRequired:false, type: 'text'},
      {name:'sum',  short_title: 'Сумма', title: 'Сумма', isRequired:false, type: 'text'},

      {name:'payment',  short_title: 'Оплачено', title: 'Оплачено', isRequired:false, type: 'select'},
      {name: 'payment_type',  short_title: 'Тип оплаты', title: 'Тип оплаты', isRequired:false, type: 'select'},

    ],
    
  }),
  getters: {
    CLIENT_INPUT_ITEMS(state){
      return state.clientInputItems;
    },
    CONTRACT_INPUT_ITEMS(state){
        return state.contractInputItems;
      },
  },
  mutations: {
  },
  actions: {
   },
}
