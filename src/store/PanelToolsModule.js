export const PanelToolsModule = {
    state: () => ({
        clientsToolsBar: [
            {name: 'Добавить', ico: 'fa-solid fa-circle-plus', action: 'add', path:'addClient'},
            {name: 'Изменить', ico: 'fa-solid fa-pen-to-square', action: 'edit', path:'clients'},
            {name: 'Удалить', ico: 'fa-solid fa-trash-can', action: 'delete', path:'clients'},
            {name: 'К договорам', ico: 'fa-solid fa-file', action: 'contracts', path:'contracts'},
            // {name: 'К гос.закупкам', ico: 'fa-sharp fa-solid fa-ruble-sign', action: 'contracts', path:'contracts'},
        ],
        contractsToolsBar: [
            {name: 'Добавить', ico: 'fa-solid fa-circle-plus', action: 'add', path:'addContract'},
            {name: 'Изменить', ico: 'fa-solid fa-pen-to-square', action: 'edit', path:'contracts'},
            {name: 'Удалить', ico: 'fa-solid fa-trash-can', action: 'delete', path:'contracts'},
            {name: 'В архив', ico: 'fa-solid fa-box-archive', action: 'delete', path:'contracts'},
            {name: 'К заказам', ico: 'fa-solid fa-cart-shopping', action: 'delete', path:'contracts'},
            {name: 'К работам', ico: 'fa-solid fa-handshake', action: 'delete', path:'contracts'},
            {name: 'К логистике', ico: 'fa-solid fa-truck-fast', action: 'delete', path:'contracts'},
            {name: 'Все/свои', ico: 'fa-solid fa-people-group', action: 'delete', path:'contracts'},
            {name: 'Архив/актив', ico: 'fa-solid fa-chart-line', action: 'delete', path:'contracts'},
        ],
    }),
    getters: {
        CLIENTS_TOOL_BAR(state){
            return state.clientsToolsBar;
        },
        CONTRACTS_TOOL_BAR(state){
            return state.contractsToolsBar
        },
    },
    mutations: {
    },
    actions: {
    },
    modules: {
    }
  }
