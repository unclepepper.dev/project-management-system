import axios from "axios";
export const ContractsItemsModule = {
    state: () => ({
        contractslistItems: []
    }),
    getters: {
        CONTRACTS_LIST_ITEMS(state){
            return state.contractslistItems;
        },
        CONTRACTS_LIST_ITEMS_COUNT(state){
            return state.contractslistItems.length
        },
    },
    mutations: {
        SET_CONTRACTS_LIST_ITEM_TO_STATE: (state, listItems) => {
            state.contractslistItems = listItems;
        },
        ADD_CONTRACTS_LIST_ITEM_TO_STATE: (state, arr) => {
            state.contractslistItems.unshift({ ...arr});
        },
        IS_CHECKED_CHANGE_CONTRACTS_LIST_ITEM_TO_STAGE: (state, id) => {
            const contractslistItems = state.contractslistItems.find((item) => item.id === id)
            contractslistItems.isChecked = !contractslistItems.isChecked;
        }
    },
    actions: {
        async GET_CONTRACTS_LIST_ITEMS_FROM_API({commit}){
            try {
                const listItems = await axios('http://localhost:3000/contractslistItems', {
                    method: "GET"
                });
                commit('SET_CONTRACTS_LIST_ITEM_TO_STATE', listItems.data);
                return listItems;
            } catch (error) {
                console.log(error);
                return error;
            }
        },  
    },
}
