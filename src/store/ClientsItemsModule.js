import axios from "axios";
export const ClientsItemsModule = {
    state: () => ({
        listItems: []
    }),
    getters: {
        LIST_ITEMS(state){
            return state.listItems;
        },
        LIST_ITEMS_COUNT(state){
            return state.listItems.length
        },
        
    },
    mutations: {
        SET_LIST_ITEM_TO_STATE: (state, listItems) => {
            state.listItems = listItems;
        },
        ADD_LIST_ITEM_TO_STATE: (state, arr) => {
            state.listItems.unshift({ ...arr});
        },
        IS_CHECKED_CHANGE_LIST_ITEM_TO_STAGE: (state, id) => {
            const listItems = state.listItems.find((item) => item.id === id)
            listItems.isChecked = !listItems.isChecked;
        }
    },
    actions: {
        async GET_LIST_ITEMS_FROM_API({commit}){
            try {
                const listItems = await axios('http://localhost/api/client', {
                    method: "GET",
                },
                {origin: true, credentials: true}
                );
                commit('SET_LIST_ITEM_TO_STATE', listItems.data);
                return listItems;
            } catch (error) {
                console.log(error);
                return error;
            }
        },
   
        
    },
}
