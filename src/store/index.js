import { createStore } from 'vuex';
import {EntryDataModule} from './EntryDataModule';
import {PanelToolsModule} from './PanelToolsModule';
import {MenuModule} from './MenuModule';
import {ClientsItemsModule} from './ClientsItemsModule';
import {ContractsItemsModule} from './ContractsItemsModule';



export default createStore({
    modules: {
    entryData: EntryDataModule,
    panelTools: PanelToolsModule,
    menu: MenuModule,  
    clientsItems: ClientsItemsModule,   
    contractsItems: ContractsItemsModule 
    }
})
