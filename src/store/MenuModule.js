export const MenuModule = {
    state: () => ({
        navMenu: [
            {name: 'Клиенты', slug: 'clients'},
            {name: 'Договоры', slug: 'contracts'},
            {name: 'Продления', slug: 'extensions' },
            {name: 'Задачи', slug: 'tasks' },
            {name: 'Закупки', slug: 'purchases' },
            {name: 'Работы', slug: 'works' },
            {name: 'Тех. поддержка', slug: 'support' },
            {name: 'Документация', slug: 'docs' },
            {name: 'Логистика', slug: 'logistic' },
            // {name: 'Финансы', slug: 'finance' },
            // {name: 'Дебиторка', slug: 'receivable' },
        ],
    }),
    getters: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
    }
  }
